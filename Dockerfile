FROM python:3.9
ENV PYTHONUNBUFFERED 1

RUN apt-get update && \
  apt-get install -y python3-pip python3-dev libpq-dev python-dev

WORKDIR /code
COPY . /code/
RUN mkdir -p /code/logs
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
