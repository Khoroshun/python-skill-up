# Django skill up project

Skill up project with Django and Django rest framework

## Getting Started

## Prerequisites

### Docker 

Install [Docker](https://docs.docker.com/get-docker/)

Create the docker group

```
sudo groupadd docker
sudo usermod -aG docker $USER
```

Reboot to apply changes

### Docker Compose 

Install [Docker Compose](https://docs.docker.com/compose/install/)


## Steps for initial project setup

Open console. Change folder to the project's root folder and run commands below

Prepare environment file. Copy environment file from template and edit variables to your needs

```
cp .env .env.local
```

Setup development environment by runing next commands

```
docker-compose up -d
```


## Daily project usage (after initial project setup)

start the project
```
docker-compose up -d
```

stop the project
```
docker-compose down
```

## Project resources


Project http://127.0.0.1:8000/

Django administration http://127.0.0.1:8000/admin/


## Running the tests

Explain how to run the automated tests for this system


## Requirements

[requirements.txt](requirements.txt)
